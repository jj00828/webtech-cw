# TwoCents: Advanced Challenges in Web Technologies

Coursework 2

The active branch structure will be as follows:

**auth-service:** Authentication Microservice
**chat-service:** Chatting Backend Microservice
**frontend-service:** Chatting Frontend Microservice

[Login Page](https://project6940.ew.r.appspot.com/)
Login Credentials:
**Email:** test@surrey.ac.uk
**Password:** password
